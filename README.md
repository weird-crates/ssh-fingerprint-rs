# SSH Fingerprint Crate

Wrapper for `ssh-keygen -l -f <FILE>` command.

## How to use

Attach dependency:

```toml
ssh-fingerprint-rs = { version = "0.1.0", git = "https://gitlab.com/weird-crates/ssh-fingerprint-rs.git"}
```

**Functions:**

- `get_public_key_fingerprints_from_file`

**Structs:**

```rust
#[derive(Serialize,Deserialize,PartialEq,Debug,Clone)]
pub struct PublicKeyFingerprint {
    /// Example: 2048, 4096
    pub key_length: u32,

    /// Example: SHA256
    pub fingerprint_type: String,

    /// Example: jNBr1DTFKFbZo3wzKIkWRIAZ2ZaLJDF6yv5qkRMYMpg
    pub fingerprint: String,

    /// Example: rfeynman@science.com
    pub key_id: String,

    /// Example: RSA
    pub key_type: String,
}
```


**Errors:**

Struct `SshFingerprintError`.
