use std::fmt::{Display, Formatter};
use std::process::Command;

use log::{debug, error, info};
use regex::Regex;
use serde::{Deserialize, Serialize};

use crate::error::SshFingerprintError;
use crate::error::SshFingerprintError::CommandExecutionError;

pub mod error;

#[derive(Serialize,Deserialize,PartialEq,Debug,Clone)]
pub struct PublicKeyFingerprint {
    /// Example: 2048, 4096
    pub key_length: u32,

    /// Example: SHA256
    pub fingerprint_type: String,

    /// Example: jNBr1DTFKFbZo3wzKIkWRIAZ2ZaLJDF6yv5qkRMYMpg
    pub fingerprint: String,

    /// Example: rfeynman@science.com
    pub key_id: String,

    /// Example: RSA
    pub key_type: String,
}

impl Display for PublicKeyFingerprint {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {}:{} {} ({})", self.key_length, self.fingerprint_type, self.fingerprint, self.key_id, self.key_type)
    }
}

pub fn get_public_key_fingerprints_from_file(file_path: &str) -> Result<Vec<PublicKeyFingerprint>, SshFingerprintError> {
    info!("get key fingerprints from file '{file_path}'");
    let args = format!("-l -f {file_path}");

    let stdout = execute_cmd("ssh-keygen", &args)?;

    let pattern = Regex::new("^(?P<size>\\d+)\\s(?P<digest_type>[A-Z0-9]+):(?P<fingerprint>[a-zA-Z0-9/+]+)\\s(?P<key_id>.*)\\s\\((?P<key_type>[a-zA-Z0-9]+)\\)$").expect("unexpected error, invalid regexp");

    let rows = stdout.split("\n").collect::<Vec<&str>>();

    let mut fingerprints: Vec<PublicKeyFingerprint> = vec![];

    for row in rows {
        if pattern.is_match(&row) {
            for caps in pattern.captures_iter(&row) {
                let key_size = format!("{}", &caps["size"]);
                let digest_type = format!("{}", &caps["digest_type"]);
                let key_id = format!("{}", &caps["key_id"]);
                let fingerprint = format!("{}", &caps["fingerprint"]);
                let key_type = format!("{}", &caps["key_type"]);

                let pub_key_fingerprint = PublicKeyFingerprint {
                    key_length: key_size.parse()?,
                    fingerprint_type: digest_type.to_string(),
                    fingerprint: fingerprint.to_string(),
                    key_id,
                    key_type,
                };

                info!("add fingerprint '{}'", pub_key_fingerprint);

                fingerprints.push(pub_key_fingerprint);

                break;
            }
        }
    }

    Ok(fingerprints)
}

fn execute_cmd(cmd: &str, args: &str) -> Result<String, SshFingerprintError> {
    let args: Vec<&str> = args.split(" ").collect();

    let output = Command::new(&cmd).args(args).output()?;

    if output.status.success() {
        let stdout = format!("{}", String::from_utf8_lossy(&output.stdout));

        debug!("<stdout>");
        debug!("{}", stdout);
        debug!("</stdout>");

        Ok(stdout)

    } else {
        error!("command execution error");
        let stderr = String::from_utf8_lossy(&output.stderr);

        error!("<stderr>");
        error!("{}", stderr);
        error!("</stderr>");

        Err(CommandExecutionError)
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;

    use crate::{get_public_key_fingerprints_from_file, PublicKeyFingerprint};

    /*
        $ ssh-keygen -l -f test-data/authorized_keys
        2048 SHA256:aOi4x5Ivji0OJ9HOGgJzI5S3HtYP14qioov+24Z09h0 vtx20@gtech.de (RSA)
        2048 SHA256:TRviLm4gENEMKStqrR9qhN/mC5l3JLOpCdzuW9oKd3M f.swamps@gtech.de (RSA)
        2048 SHA256:SDXVIAq3U3sv5LKsg6zFvA8OGC+zLaN7SDXg6BstGrw rfeynman@science.us (RSA)
        2048 SHA256:Hd9KxHTMv8nWU2oGhgLFwoIf+ufiojJa/xtrSnoW9wY alexander@Alexanders-MacBook-Pro.local (RSA)
        2048 SHA256:jTOS5Er1OGdBpepQG/NsElBfxCZRj/KlxG3ZzD4L6ZQ wisley@techcorp.com (RSA)
        2048 SHA256:jNBr1DTFKFbZo3wzKIkWRIAZ2ZaLJDF6yv5qkRMYMpg j.patterson@company.com (RSA)
         */
    #[test]
    fn fingerprints_should_be_returned() {
        let fingerprint1 = get_expected_fingerprint("aOi4x5Ivji0OJ9HOGgJzI5S3HtYP14qioov+24Z09h0", "vtx20@gtech.de");
        let fingerprint2 = get_expected_fingerprint("TRviLm4gENEMKStqrR9qhN/mC5l3JLOpCdzuW9oKd3M", "f.swamps@gtech.de");
        let fingerprint3 = get_expected_fingerprint("SDXVIAq3U3sv5LKsg6zFvA8OGC+zLaN7SDXg6BstGrw", "rfeynman@science.us");
        let fingerprint4 = get_expected_fingerprint("Hd9KxHTMv8nWU2oGhgLFwoIf+ufiojJa/xtrSnoW9wY", "alexander@Alexanders-MacBook-Pro.local");
        let fingerprint5 = get_expected_fingerprint("jTOS5Er1OGdBpepQG/NsElBfxCZRj/KlxG3ZzD4L6ZQ", "wisley@techcorp.com");
        let fingerprint6 = get_expected_fingerprint("jNBr1DTFKFbZo3wzKIkWRIAZ2ZaLJDF6yv5qkRMYMpg", "j.patterson@company.com");
        let expected_fingerprints = vec![fingerprint1, fingerprint2, fingerprint3, fingerprint4, fingerprint5, fingerprint6];

        let path = Path::new("test-data").join("authorized_keys");
        let path_str = format!("{}", path.display());

        let fingerprints = get_public_key_fingerprints_from_file(&path_str).unwrap();

        for expected_fingerprint in expected_fingerprints {
            assert!(fingerprints.iter().find(|f| *f.fingerprint == expected_fingerprint.fingerprint).is_some());
        }
    }

    #[test]
    fn return_error_for_unknown_file() {
        let path = Path::new("test-data").join("unknown-file");
        let path_str = format!("{}", path.display());
        assert!(get_public_key_fingerprints_from_file(&path_str).is_err());
    }

    fn get_expected_fingerprint(fingerprint: &str, key_id: &str) -> PublicKeyFingerprint {
        PublicKeyFingerprint {
            key_length: 2048,
            fingerprint_type: "SHA256".to_string(),
            fingerprint: fingerprint.to_string(),
            key_id: key_id.to_string(),
            key_type: "RSA".to_string(),
        }
    }
}
