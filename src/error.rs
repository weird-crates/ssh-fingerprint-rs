use std::io;
use std::num::ParseIntError;

use thiserror::Error;

#[derive(Error, Debug)]
pub enum SshFingerprintError {
    #[error("io error")]
    IOError(#[from] io::Error),

    #[error("integer parse error")]
    ParseIntError(#[from] ParseIntError),

    #[error("unsupported format")]
    UnsupportedFormat,

    /// Got error after attempt to execute ssh-keygen
    #[error("command execution error")]
    CommandExecutionError
}
